package org.usfirst.frc.team5316.robot;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.hal.ConstantsJNI;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.swing.SpringLayout.Constraints;

import org.usfirst.frc.team5316.robot.Robot.MonitorThread;

//import org.usfirst.frc.team5316.robot.Robot.MonitorThread;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.can.*;

//Debug writing libaries
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.time.Duration;
import java.time.Instant;

public class Robot extends IterativeRobot {

	//Hardware
	//Talons
	public TalonSRX motorDriveRightSide;
	public TalonSRX motorDriveLeftSide;
	public TalonSRX motorForkLift;	//Move to the forkContol class.
	//public TalonSRX motorIntake;	//Motor at the end of the forks
	public TalonSRX motorLift;		//Lift Motor for elivation.
	public VictorSPX motorIntake;
	//Phnomatic Controller.
	Compressor curCompressor;
	
	//Solenoids
	//Solenoid curSolenoid;
	//Solenoid curS2;
	//ArrayList<DoubleSolenoid> curDoubleSolenoid = new ArrayList<DoubleSolenoid>();		//All double solenoids
	
	//Coefficients
	public double[] coefMotorCorrection;
	public double[] coefAcceleration;
	
	//Controllers
	private Joystick controllerDriving;		//Controller for the movement of the vehicle
	private Joystick controllerForkLift;	//Controller for the movement of the fork and control of the input
	
	//Controller Settings Items
	public double deadband = .3;
	
	//Forklift Movement Controller.
	public contolForkLift equipmentForkLift;	//Object for controlling the forklift
	
	//Setting up independent thread
	MonitorThread _monitor;
	
	//Logging objects
	File f;
	BufferedWriter bw;
	FileWriter fw;
	
	boolean runThread;
	
	//Correction coef
	double[] rightMotorCoef = new double[] {1.8239, -4.0123, 2.8389, 0.1335, 0.0649};
	double[] drivePowerScaleCoef = new double[] {-0.00613, 0.16817, 0.20544, 0.83558, -0.19862, -0.30422};
	
	//Controler settings.
	int conDriverLeftStickAxis = 1;
	int conDriverRightStickAxis = 5;
	
	int conForkLeftStickAxis = 1;
	int conForkRightStickAxis = 5;

	//Buffer for movement
	double[] leftSpeedBuffer = new double[7];
	double[] rightSpeedBuffer = new double[7];
	int counterLeftSpeed = 0;
	int counterRightSpeed = 0;
	
	//Game state data.
	String gameData = ""; //Setting to something that will not be used.
	Boolean runGameThread = true;
	StartConditionThread gameThread;
	
	//Scale limit for the controller
	double inputScale = .4;
	
	//Navigation
	Navigation nav;
	
	vehicle curVehicle;
	
	
	/////////////////////////////////////////////////////////
	//Starting Pos
	////////////////////////////////////////////
	//1 left
	//2 center
	//3 right
	//4 Stringt and stop
	public int startPos = 4;
	
	
	
	
	
	
	
	public Instant startOfAton;
	
	@Override
	public void robotInit() {
		//Setup Hardware
		initHardware();		//Starting up all hardware.
		
		initSpeed();     //setting up the buffer.
		
		//Setting up the thread that will check the game data until we want to stop.
		//gameThread = new StartConditionThread(this);
		//new Thread(gameThread).start();
		
	}
	
	
    public void teleopInit(){ 
    	
    	setupLogging();
    	
    	runThread = true;
    	
    	//Setting up the monitor thread.
		_monitor = new MonitorThread(this);
		new Thread(_monitor).start();

		//Start Compressor contol loop.
    	//curCompressor.setClosedLoopControl(true);
    	
    	
    }
    
    public void disabledInit() {
    	runThread = false;
    	
    }
    
	public void autonomousInit() {
		startOfAton = Instant.now();
		
		setupLogging();
		
		setLeftMotorSpeed(0);
		setRightMotorSpeed(0);
		
		runThread = true;
    	
    	//Setting up the monitor thread.
		_monitor = new MonitorThread(this);
		new Thread(_monitor).start();
		
		curVehicle = new vehicle();
		
		nav = new Navigation(curVehicle, this);
		//nav.loadDesiredPath("LRL", startPos);  //Load nav settings.
		
		
		System.out.println("Game Data Before=-" + DriverStation.getInstance().getGameSpecificMessage() + "-");
		
		
		while(DriverStation.getInstance().getGameSpecificMessage().length() < 2 || gameData == "") {
			System.out.println("Game Data=-" + DriverStation.getInstance().getGameSpecificMessage() + "-");
			gameData = DriverStation.getInstance().getGameSpecificMessage();
			if(DriverStation.getInstance().getGameSpecificMessage().length() > 2) {
				nav.loadDesiredPath(gameData, startPos);
				nav.startNav();
				break;
			}
			
			//Getting time passed.
			Instant curTime = Instant.now();
			Duration timeElapsed = Duration.between(startOfAton, curTime);
			
			double secPassed = timeElapsed.getSeconds();
			
			if(secPassed > 5) {
				nav.loadDesiredPath("RRR", 4);
				nav.startNav();
				break;
			}
			
		}
		
		//Set starting position. 1,2,3
		//int startingPOS = 1;
		//nav.startNav();
		
		
	}
	
	public void testInit() {
        if(motorDriveRightSide != null && motorDriveLeftSide != null) {
            controlDriving();
        }else {
            System.out.println("Drive Motors Offline");
        }
        
        
        if(motorForkLift != null) {
            contolFork();
        }else {
            System.out.println("Forlift Motors Offline");
        }
        
        
        if(motorIntake != null) {
            contolForkInput();
        }else {
            System.out.println("Intake Motors Offline");
        }
        /*
        if(motorLift != null) {
            controlLift();
        }else {
            System.out.println("Lift Motor Offline");
        }
        */
        //curCompressor.start();    //Starting compressor
        
        //boolean enabled = curCompressor.enabled();
        //boolean pressureSwitch = curCompressor.getPressureSwitchValue();
        //double current = curCompressor.getCompressorCurrent();
        
        //System.out.println(enabled + "," + pressureSwitch + "," + current);

		
	}
    
	public void teleopPeriodic() {		
		
		if(motorDriveRightSide != null && motorDriveLeftSide != null) {
            controlDriving();
        }else {
            System.out.println("Drive Motors Offline");
        }
        
        
        if(motorForkLift != null) {
            contolFork();
        }else {
            System.out.println("Forlift Motors Offline");
        }
        
        
        if(motorIntake != null) {
            contolForkInput();
        }else {
            System.out.println("Intake Motors Offline");
        }
        /*
        if(motorLift != null) {
            controlLift();
        }else {
            System.out.println("Lift Motor Offline");
        }
        */
        //curCompressor.start();    //Starting compressor
        
        //boolean enabled = curCompressor.enabled();
        //boolean pressureSwitch = curCompressor.getPressureSwitchValue();
        //double current = curCompressor.getCompressorCurrent();
        
        //System.out.println(enabled + "," + pressureSwitch + "," + current);
	}
	
	
	public void autonomousPeriodic() {
		
		curVehicle.curLeftSpeed = motorDriveLeftSide.getMotorOutputPercent();
		curVehicle.curRightSpeed = motorDriveRightSide.getMotorOutputPercent();
			
		
		
		//Pushing data to the nav system for speed and direction corrections.
		//nav.calcPos(this.motorDriveLeftSide.getSelectedSensorPosition(0), this.motorDriveRightSide.getSelectedSensorPosition(0));
		nav.calcPos(this.motorDriveRightSide.getSelectedSensorPosition(0), this.motorDriveLeftSide.getSelectedSensorPosition(0));
		
	}
	
	//Shutdown code area
	public void disabledPeriodic() {
		
	}
	
	public void robotPeriodic() {
		
	}
		
	public void testPeriodic() {
		teleopPeriodic();
	}
	
	//Method to bring all hardware online.
	private void initHardware() {
		
		
		//Motor controllers (PWM)
		motorDriveRightSide = new TalonSRX(0);		//Setting the id
		motorDriveRightSide.setInverted(true);		//Setting the direction
		motorDriveRightSide.set(ControlMode.PercentOutput, 0);		//Setting the power to 0
		//Motor Encoders
		motorDriveRightSide.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 1, 10);
		motorDriveRightSide.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
		
		motorDriveLeftSide = new TalonSRX(1);		//Setting the id
		motorDriveLeftSide.setInverted(false);		//Setting the direction
		motorDriveLeftSide.set(ControlMode.PercentOutput, 0);		//Setting the power to 0
		//Motor Encoders
		motorDriveLeftSide.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 1, 10);
		motorDriveLeftSide.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
		
		//Fork lift motor controller.
		
		motorForkLift = new TalonSRX(5);
		motorForkLift.setInverted(false);	
		motorForkLift.set(ControlMode.PercentOutput, 0);
		
		//Motor Encoders
		//motorForkLift.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 1, 10);
		//motorForkLift.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
		
		//motorIntake = new TalonSRX(3);	//Forklift Input setup
		motorIntake = new VictorSPX(3);	
		motorIntake.set(ControlMode.PercentOutput, 0);
		
		//motorLift = new TalonSRX(4);
		//motorLift.set(ControlMode.PercentOutput, 0);
			
		//Setting up the controllers.
		controllerDriving = new Joystick(0);		//Controller for driving the vehicle
		controllerForkLift = new Joystick(1);		//Controller for the forlift.
		
		//Setting up the forklift class.
		equipmentForkLift = new contolForkLift(motorForkLift);
		
		//Setting up the compressor
		//curCompressor = new Compressor(0);
		//curCompressor.setClosedLoopControl(true);		//Setting the comporesser so closed loop (auto)
		
		//Setting up the Solenoids
		/*
		for( int i = 0; i< 8; i++) {			
			curDoubleSolenoid.add(new DoubleSolenoid(i, i+1));			
			i++;
		}
		*/
	}
	
	private void initSpeed() {
		for(int i = 0; i< leftSpeedBuffer.length; i++) {
			leftSpeedBuffer[i] = 0;
		}
		
		for(int x = 0; x< rightSpeedBuffer.length; x++) {
			rightSpeedBuffer[x] = 0;
		}
	}
	
	//Loads all settings.
	private void loadAllSettings() {
		
	}
	
	private void setupLogging() {
		
		try {
    		f = new File("/home/lvuser/" + LocalDateTime.now().toString() + "-Output.csv");
    		if(!f.exists()){
    			f.createNewFile();
    		}
			fw = new FileWriter(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	bw = new BufferedWriter(fw);
    	
    	
		String header = "DateTime,";
		if(motorDriveLeftSide != null) {
			header += "mDLSVOL,mDLSPOS,mDLSPWR,";
		}
		if(motorDriveRightSide != null) {
			header += "mDRSVOL,mDRSPOS,mDRSPWR,";
		}
		if(motorForkLift != null) {
			header += "mFLVOL,mFLPOS,mFLPWR,";
		}
		if(motorIntake != null) {
			header += "mInPWR,";
		}
		if(motorLift != null) {
			header += "mLTPWR,";
		}
		if(controllerDriving != null) {
			header += "conDLS,conDRS,";
		}
		if(controllerForkLift != null) {
			header += "conFLS,conFRS,";
		}
		if(equipmentForkLift != null) {
			header += "ForkCount,";
		}
		
		header += "\r\n";
		
		try {
    		fw = new FileWriter(f,true);
    		bw = new BufferedWriter(fw);
			//bw.write(dataLine);
    		bw.append(header);
			bw.close();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
	}
	
	private void controlDriving() {
		//Setting up containers for stick values. So they can be tested.
		//double stickLeftValue = -.4 + (-.4*.48);// controllerDriving.getRawAxis(conDriverLeftStickAxis);
		//double stickRightValue = -.4; // controllerDriving.getRawAxis(conDriverRightStickAxis);
		
		double stickLeftValue = controllerDriving.getRawAxis(conDriverLeftStickAxis) + (controllerDriving.getRawAxis(conDriverLeftStickAxis) * .15);
		double stickRightValue = controllerDriving.getRawAxis(conDriverRightStickAxis);
		
		//stickLeftValue = correctScale(stickLeftValue);
		//stickRightValue = correctScale(stickRightValue);
		
		//Scaling the control to the scaler
		stickLeftValue = scaleInput(stickLeftValue);
		stickRightValue = scaleInput(stickRightValue);
		
		leftSpeedBuffer[counterLeftSpeed] = stickLeftValue;
		rightSpeedBuffer[counterRightSpeed] = stickRightValue;
		
		
		counterLeftSpeed++;
		counterRightSpeed++;
		
		//Resetting the counter.
		if(counterLeftSpeed >= leftSpeedBuffer.length) {
			counterLeftSpeed = 0;
		}
		
		//Resetting the counter.
		if(counterRightSpeed >= rightSpeedBuffer.length) {
			counterRightSpeed = 0;
		}
		
		//Getting the average
		for(int i = 0; i< leftSpeedBuffer.length; i++) {
			stickLeftValue += leftSpeedBuffer[i];
		}
		
		for(int i = 0; i< rightSpeedBuffer.length; i++) {
			stickRightValue += rightSpeedBuffer[i];
		}
		
		stickLeftValue = stickLeftValue/leftSpeedBuffer.length;
		stickRightValue = stickRightValue/rightSpeedBuffer.length;
		
		//System.out.println(correctRightMotor(stickRightValue));
		
		//Correcting the right motor power setting.
		//stickRightValue = correctRightMotor(stickRightValue);
		
		//System.out.println(stickLeftValue + "," + stickRightValue);
		
		//This needs to be replaced with the limter and scaler equation. and motor correction.
		//Testing for dead bands. We mght need to find better controllers.
		
		if(stickLeftValue < scaleInput(0.15) && stickLeftValue > scaleInput(-0.15)) {
			stickLeftValue = 0;
		}
		
		if(stickRightValue < scaleInput(0.15) && stickRightValue > scaleInput(-0.15)) {
			stickRightValue = 0;
		}
		
		
		
		//Sending the percentage desired to the motor controller.
		setLeftMotorSpeed(stickLeftValue);
		setRightMotorSpeed(stickRightValue);
		
	}
	
	public void setLeftMotorSpeed(double setpoint) {
		motorDriveLeftSide.set(ControlMode.PercentOutput, setpoint);
	}
	
	public void setRightMotorSpeed(double setpoint) {
		motorDriveRightSide.set(ControlMode.PercentOutput, setpoint);
	}

	//Scale controller to the top limit
	double scaleInput(double input) {
		return input * inputScale;
	}
	
	
	
	//Correct the right motor to balance to the left
	private double correctRightMotor(double input) {
		
		return rightMotorCoef[0] * Math.pow(input, 4) +
				rightMotorCoef[1] * Math.pow(input, 3) + 
				rightMotorCoef[2] * Math.pow(input, 2) +
				rightMotorCoef[3] * Math.pow(input, 1) +
				rightMotorCoef[4];
	}
	
	public double correctScale(double input) {
		double value = 0;
		
		for(int i = drivePowerScaleCoef.length - 1; i>= 0; i-- ) {
			value += drivePowerScaleCoef[i] * Math.pow(input, i);
		}
		return value;
	}
	
	private void contolFork() {
		double stickLeftValue = controllerForkLift.getRawAxis(conForkLeftStickAxis) / 1; //Getting the for control axis value.
		
		//If the stick value is 0 stop the lift.
		if(stickLeftValue == 0) {
			equipmentForkLift.stopFork();
		}
		else {
			if(equipmentForkLift.isItMoving()) {
				equipmentForkLift.updateFork(stickLeftValue);
			}
			else {
				equipmentForkLift.moveFork(stickLeftValue);
			}
		}
		
	}
	
	private void contolForkInput() {
		if(controllerForkLift.getRawButton(1) == true) {
			setInletSpeed(1);
		}
		
		if(controllerForkLift.getRawButton(2) == true) {
			setInletSpeed(-1);
		}
		
		if(controllerForkLift.getRawButton(3) == true) {
			setInletSpeed(0);
		}
		
		/*
		if(controllerForkLift.getRawButton(5) == true) {
			curDoubleSolenoid.get(0).set(DoubleSolenoid.Value.kForward);
		}
		
		if(controllerForkLift.getRawButton(6) == true) {
			curDoubleSolenoid.get(0).set(DoubleSolenoid.Value.kReverse);
		}
		*/
		
	}
	
	public void setInletSpeed(double speed) {
		motorIntake.set(ControlMode.PercentOutput, speed);
	}
	
	private void controlLift() {
		double stickRightValue = controllerForkLift.getRawAxis(1);
		
		//motorLift.set(ControlMode.PercentOutput, stickRightValue);
	}
	
	private void controlPneumatics() {
		/*
		exampleDouble.set(DoubleSolenoid.Value.kOff);
		exampleDouble.set(DoubleSolenoid.Value.kForward);
		exampleDouble.set(DoubleSolenoid.Value.kReverse);
		*/
	}
	
	class MonitorThread implements Runnable {
		
		Robot robot;
		
		public MonitorThread(Robot robot) {
			this.robot = robot;
		}
		
		public void run() {
			
			try {
				this.robot.motorDriveLeftSide.setSelectedSensorPosition(0,0,0);	//Resets motor controller to 0 pos.
				this.robot.motorDriveRightSide.setSelectedSensorPosition(0,0,0);	//Resets motor controller to 0 pos.
				this.robot.motorForkLift.setSelectedSensorPosition(0, 0, 0);	//Resetting the motor controller to ps.
			}
			catch (Exception err)
			{}
			
			
			while(runThread) {
				
				//System.out.println("Thread Alive");
				
				//Log Data container
				String dataLine = LocalDateTime.now().toString() + ",";
				
				//dataLine = LocalDateTime.now().toString() + "," + dataLine;
				
				//Left Drive motor
				if(motorDriveLeftSide != null) {
					try {
						dataLine += Double.toString(this.robot.motorDriveLeftSide.getSelectedSensorVelocity(0)) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
					
					try {
						dataLine += Double.toString(this.robot.motorDriveLeftSide.getSelectedSensorPosition(0)) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
					
					try {
						dataLine += Double.toString(this.robot.motorDriveLeftSide.getMotorOutputPercent()) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
				}
				
				//Right drive motor
				if(motorDriveRightSide != null) {
					try {
						dataLine += Double.toString(this.robot.motorDriveRightSide.getSelectedSensorVelocity(0)) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
						
					try {
						dataLine += Double.toString(this.robot.motorDriveRightSide.getSelectedSensorPosition(0)) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
					
					try {
						dataLine += Double.toString(this.robot.motorDriveRightSide.getMotorOutputPercent()) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
				}
				//ForkLift Motor
				if(motorForkLift != null) {
					try {
						dataLine += Double.toString(this.robot.motorForkLift.getSelectedSensorVelocity(0)) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
					
					try {
						dataLine += Double.toString(this.robot.motorForkLift.getSelectedSensorPosition(0)) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
					
					try {
						dataLine += Double.toString(this.robot.motorForkLift.getMotorOutputPercent()) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
					
					//For checking.
					//System.out.println(this.robot.motorForkLift.getSelectedSensorVelocity(0) + "," + this.robot.motorForkLift.getSelectedSensorPosition(0));
				}
				//Intake Motors
				if(motorIntake != null) {
					try {
						dataLine += Double.toString(this.robot.motorIntake.getMotorOutputPercent()) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
				}
				//Lifting Motor
				if(motorLift != null) {
					try {
						dataLine += Double.toString(this.robot.motorLift.getMotorOutputPercent()) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
				}
				
				if(controllerDriving != null) {
					try {
						dataLine += controllerDriving.getRawAxis(conDriverLeftStickAxis) + "," + controllerDriving.getRawAxis(conDriverRightStickAxis) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
				}
				if(controllerForkLift != null) {
					try {
						dataLine +=  controllerForkLift.getRawAxis(conForkLeftStickAxis) + "," + controllerForkLift.getRawAxis(conForkRightStickAxis) + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
				}
				
				if(equipmentForkLift != null) {
					try {
						dataLine += equipmentForkLift.getCurrentPos() + ",";
					} catch (Exception err) {
						dataLine += "0,";
					}
				}
				
				//Adding line end to the data.
				dataLine += "\r\n";
				
				//Composing the header.
				if(!f.exists()) {
					String header = "DateTime,";
					if(motorDriveLeftSide != null) {
						header += "mDLSVOL,mDLSPOS,mDLSPWR,";
					}
					if(motorDriveRightSide != null) {
						header += "mDRSVOL,mDRSPOS,mDRSPWR";
					}
					if(motorForkLift != null) {
						header += "mFLPWR,";
					}
					if(motorIntake != null) {
						header += "mInPWR,";
					}
					if(motorLift != null) {
						header += "mLTPWR,";
					}
					if(controllerDriving != null) {
						header += "conDLS,conDRS,";
					}
					if(controllerForkLift != null) {
						header += "conFLS,conRRS,";
					}
					if(equipmentForkLift != null) {
						header += "ForkCount,";
					}
					
					//Adding the new line to the end.
					writeToFile(header + "\r\n");
					
				} else {
					writeToFile(dataLine);
				}
				
				//A break to not overrun.
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		private void writeToFile(String dataLine) {
			try {
	    		fw = new FileWriter(f,true);
	    		bw = new BufferedWriter(fw);
				//bw.write(dataLine);
	    		bw.append(dataLine);
				bw.close();
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	class StartConditionThread implements Runnable {
		
		Robot robot;
		
		
		public StartConditionThread(Robot robot) {
			this.robot = robot;
		}
		
		public void run() {
			
			while(this.robot.runGameThread) {
				
				if(!robot.gameData.equals(DriverStation.getInstance().getGameSpecificMessage())) {
					robot.gameData = DriverStation.getInstance().getGameSpecificMessage();
					System.out.println("Loading Settings");
					robot.nav.loadDesiredPath(robot.gameData, robot.startPos);
					robot.nav.startNav();
				}
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
		
	
}
