package org.usfirst.frc.team5316.robot;


import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Timer;

//Debug writing libaries
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Navigation { //implements eventMovementListener {
	private vehicle currentVehicle;		//The current vehicle navigating.
	private ArrayList<dataPath> desiredPath = new ArrayList<>();	//Container for the path desired.
	private int currentDataPoint = 0;		//The current point to be heading to.
	
	//Last data
	private double lastX;
	private double lastY;
	private double lastHeading;
	private double lastEncoderLeftCount;
	private double lastEncoderRightCount;
	
	private int startingPos = 0;
	
	//simVehicle testV;
	Robot curBot;
	
	DecimalFormat df = new DecimalFormat("###.##");		//Formatter.
	
	//Logging objects
	private File f;
	private BufferedWriter bw;
	private FileWriter fw;
		
	public Navigation(vehicle inVehicle) {
		this.currentVehicle = inVehicle;
		this.lastX = 0;
		this.lastY = 0;
		this.lastEncoderLeftCount = 0;
		this.lastEncoderRightCount = 0;
		
		setupLogging();
	}
	
	/*
	public Navigation(vehicle inVehicle, simVehicle testV) {
		this.testV = testV;
		this.currentVehicle = inVehicle;
		this.lastX = 0;
		this.lastY = 0;
		this.lastEncoderLeftCount = 0;
		this.lastEncoderRightCount = 0;
	}
	*/
	
	public Navigation(vehicle inVehicle, Robot robot) {
		this.curBot = robot;
		this.currentVehicle = inVehicle;
		this.lastX = 0;
		this.lastY = 0;
		this.lastEncoderLeftCount = 0;
		this.lastEncoderRightCount = 0;
		
		setupLogging();
		
		dataPath tempP = new dataPath();
		tempP.pointX = 0;
		tempP.pointY = 12;
		tempP.pointTol = 5;
		tempP.driveSpeed = -0.3;
		tempP.forkLiftHeight = -300;
		tempP.inletSpeed = 0;
		desiredPath.add(tempP);
		
		
		currentDataPoint = 0; //Starting with first point.
		
	}
	
	public void startNav() {
		currentDataPoint = 0; //Starting with first point.
		calcPos(0,0);	//Starting at the 0,0
		processSetPoint();
	}
	
	public void calcPos(double encoderLeftCount, double encoderRightCount) {
		//Inverting the input. Doing this because the current configuration has - meaning forward.
		//encoderLeftCount = encoderLeftCount * -1;
		//encoderRightCount = encoderRightCount * -1;
		
		
		//Triangle data.
		triangle moveTriangle = new triangle();
		moveTriangle.angleC = 90;

		//Getting the difference from last count.
		double diffLeftCount = encoderLeftCount - lastEncoderLeftCount;
		double diffRightCount = encoderRightCount - lastEncoderRightCount;
		
		double disLeft = this.currentVehicle.getTavel(diffLeftCount, "left");
		double disRight = this.currentVehicle.getTavel(diffRightCount, "right");
		
		String directionHeading = "";	//What direction are we heading
		//System.out.println("Left=" + disLeft + ",Right=" + disRight);
		writeLog("Left=" + disLeft + ",Right=" + disRight);
		writeLog("LSpeed=" + this.curBot.motorDriveLeftSide.getMotorOutputPercent() + "RSpeed=" + this.curBot.motorDriveRightSide.getMotorOutputPercent());
		
		//Calcing the distance traveled.
		double travelDistance = 0;
		if(disLeft > disRight) {
			directionHeading = "clockwise";
			moveTriangle.legA = disLeft - disRight;
			travelDistance = ((disLeft - disRight) / 2) + disRight;
		} else {
			directionHeading = "counterclockwise";
			moveTriangle.legA = disRight - disLeft;
			travelDistance = ((disRight - disLeft) / 2) + disLeft;
		}
		
		moveTriangle.legB = currentVehicle.wheelBase;
		moveTriangle = solveTriangle(moveTriangle);
		
		//Getting the movement at heading triangle.
		/*
		triangle newPos = new triangle();
		newPos.angleB = moveTriangle.angleB + this.currentVehicle.currentHeading;
		newPos.angleA = 90 - newPos.angleB;
		newPos.angleC = moveTriangle.angleC;
		newPos.legC = travelDistance;
		newPos = solveTriagleFromHypotenuse(newPos);
		*/
		
		//formatting the move angle based on clockwise, and counter clockwise
		if(directionHeading == "counterclockwise") {
			moveTriangle.angleB *= -1;
		}	
			
		updateCompass(moveTriangle.angleB); //Updating the direction.
		//System.out.println("Compass: " + this.currentVehicle.currentHeading);
		
		//These needs to take current heading into account for positive and negitive.
		if(this.currentVehicle.currentHeading >= 270 && this.currentVehicle.currentHeading < 360) {
			
			triangle triD = new triangle();
			triD.legC = travelDistance;
			triD.angleB = 360 - this.currentVehicle.currentHeading;
			triD = solveTriagleFromHypotenuse(triD);
			
			this.currentVehicle.currentX -= triD.legA;
			this.currentVehicle.currentY += triD.legB;
		}
		
		if(this.currentVehicle.currentHeading >= 180 && this.currentVehicle.currentHeading < 270) {
			
			triangle triD = new triangle();
			triD.legC = travelDistance;
			triD.angleC = 90;
			triD.angleB = 270 - this.currentVehicle.currentHeading;
			triD.angleA = 90 - triD.angleB;
			triD = solveTriagleFromHypotenuse(triD);			
			
			this.currentVehicle.currentX -= triD.legB;
			this.currentVehicle.currentY -= triD.legA;
		}
		
		if(this.currentVehicle.currentHeading >= 90 && this.currentVehicle.currentHeading < 180) {
			
			triangle triD = new triangle();
			triD.legC = travelDistance;
			triD.angleC = 90;
			triD.angleB = 180 - this.currentVehicle.currentHeading;
			triD.angleA = 90 - triD.angleB;
			triD = solveTriagleFromHypotenuse(triD);	
			
			this.currentVehicle.currentX += triD.legA;
			this.currentVehicle.currentY -= triD.legB;
		}
		
		if(this.currentVehicle.currentHeading >= 0 && this.currentVehicle.currentHeading < 90) {
			
			triangle triD = new triangle();
			triD.legC = travelDistance;
			triD.angleB = 90 - this.currentVehicle.currentHeading;
			triD = solveTriagleFromHypotenuse(triD);	
			
			this.currentVehicle.currentX += triD.legB;
			this.currentVehicle.currentY += triD.legA;
		}
		
			
		//Updating the last counter.
		lastEncoderLeftCount = encoderLeftCount;
		lastEncoderRightCount = encoderRightCount;
		
		
		//System.out.println("Heading: " + directionHeading + "," + df.format(this.currentVehicle.currentHeading) + " Distance: " + df.format(travelDistance) + " X=" +
		//						df.format(this.currentVehicle.currentX) + " Y=" + df.format(this.currentVehicle.currentY));
		
		//System.out.println(this.currentVehicle.currentX + "," + this.currentVehicle.currentY + ",0");
		
		//Looking at the current pos and where we want to go.
		//triangle toPoint = triangleToPoint();		//Angle B is the correction needed to get on verctor.	
		
		//Adjust motor output to get on course.
		
		if(desiredPath != null) {
			distanceAndHeading();
		}
		
		
		
	}
	
	public void distanceAndHeading() {
 		double xDiff = 0;
		double yDiff = 0;
		
		double quadAngle = 0;
		
		//Getting the difference from the desired point to the current location.
		xDiff = desiredPath.get(currentDataPoint).pointX - this.currentVehicle.currentX;
		yDiff = desiredPath.get(currentDataPoint).pointY - this.currentVehicle.currentY;
		
		
		//Find the quad the heading is in.
		if(desiredPath.get(currentDataPoint).pointX > this.currentVehicle.currentX &&
				desiredPath.get(currentDataPoint).pointY > this.currentVehicle.currentY) {
			quadAngle = 0;
		}
		
		if(desiredPath.get(currentDataPoint).pointX > this.currentVehicle.currentX &&
				desiredPath.get(currentDataPoint).pointY < this.currentVehicle.currentY) {
			
			quadAngle = 90;
		}
		
		if(desiredPath.get(currentDataPoint).pointX < this.currentVehicle.currentX &&
				desiredPath.get(currentDataPoint).pointY < this.currentVehicle.currentY) {
			quadAngle = 180;
		}
		
		if(desiredPath.get(currentDataPoint).pointX < this.currentVehicle.currentX &&
				desiredPath.get(currentDataPoint).pointY > this.currentVehicle.currentY) {
			quadAngle = 270;
		}

		
		//Finding leg c
		triangle triToPoint = new triangle();
		triToPoint.legA = Math.abs(xDiff);
		triToPoint.legB = Math.abs(yDiff);
		triToPoint.angleC = 90;
		triToPoint = solveTriangle(triToPoint);
		
		
		//System.out.println("Dist=" + df.format(triToPoint.legC) + ",Ajust=" + df.format(triToPoint.angleB) + ",Quad=" + quadAngle +
		//		" Heading=" + currentVehicle.currentHeading + " Total Correct=" + (triToPoint.angleB + currentVehicle.currentHeading));
		
		writeLog("Dist=" + df.format(triToPoint.legC) + ",Ajust=" + df.format(triToPoint.angleB) + ",Quad=" + quadAngle +
				" Heading=" + currentVehicle.currentHeading + " Total Correct=" + (triToPoint.angleB + currentVehicle.currentHeading));
		
		double corAngle = 0;
		
		//Find the overall correction angle.
		/*
		if(quadAngle == 0) {
			corAngle = (360 - currentVehicle.currentHeading) + triToPoint.angleB;
		}
		
		if(quadAngle == 270) {
			corAngle = currentVehicle.currentHeading + triToPoint.angleB;
		}
		
		if(quadAngle == 90) {
			corAngle = triToPoint.angleB + (90 - currentVehicle.currentHeading);
		}
		
		if(quadAngle == 180) {
			corAngle = triToPoint.angleB;
		}
		*/
		
		double navpointAngle = 0;
		
		if(quadAngle == 0) {
			navpointAngle = quadAngle + triToPoint.angleB;
		}
		
		if(quadAngle == 90) {
			navpointAngle = quadAngle + triToPoint.angleA;
		}
		
		if(quadAngle == 180) {
			navpointAngle = quadAngle + triToPoint.angleB;
		}
		
		if(quadAngle == 270) {
			navpointAngle = quadAngle + triToPoint.angleA;
		}
				
		
		double diff = 0;
		
		if(currentVehicle.currentHeading <= 360 && currentVehicle.currentHeading > 180) {
			diff = (360 - currentVehicle.currentHeading) + navpointAngle;
		}
		
		if(currentVehicle.currentHeading > 0 && currentVehicle.currentHeading <= 180) {
			diff = navpointAngle - currentVehicle.currentHeading;
		}
		
		
		/*
		if(navpointAngle - currentVehicle.currentHeading > 270) {
			diff = (360 - navpointAngle) + currentVehicle.currentHeading;
			diff = diff * -1;
		}
		
		if(navpointAngle - currentVehicle.currentHeading < 90) {
			diff = (90 - navpointAngle) + (360 - currentVehicle.currentHeading);
		}
		
		 
		if(diff > 360) {
			diff =  diff - 360;
		}
		
		*/
		double leftDiff = (360 - navpointAngle);
		if(currentVehicle.currentHeading >= 0 && currentVehicle.currentHeading < 180) {
			leftDiff = leftDiff + currentVehicle.currentHeading;
		} else {
			leftDiff = leftDiff - (360 - currentVehicle.currentHeading);
		}
				
		if(leftDiff < diff) {
			diff = leftDiff * -1;
		}
		
		/*
		if(diff <= -3 && testV.curLeftSpeed > desiredPath.get(currentDataPoint).driveSpeed * .15) {
			this.testV.setLeftSpeed(testV.curLeftSpeed - (testV.curLeftSpeed * 0.1));
		}
		
		if(diff >= 3 && testV.curRightSpeed > desiredPath.get(currentDataPoint).driveSpeed * .15) {
			this.testV.setRightSpeed(testV.curRightSpeed - (testV.curLeftSpeed * 0.1));
		}
		
		*/
		
		double amountRatio = Math.abs(diff) / 135;
		
		if(diff <= -3) { //&& currentVehicle.curLeftSpeed < desiredPath.get(currentDataPoint).driveSpeed * .15) {
			if(currentVehicle.curLeftSpeed  < -0.1) {
				this.curBot.setLeftMotorSpeed(currentVehicle.curLeftSpeed * (1 - amountRatio) );
			}
		}
		
		if(diff >= 3) {// && currentVehicle.curRightSpeed < desiredPath.get(currentDataPoint).driveSpeed * .15) {
			if(currentVehicle.curRightSpeed < -0.1) {
				
				this.curBot.setRightMotorSpeed(currentVehicle.curRightSpeed * (1 - amountRatio));
			}
			
		}
		
		
		if(diff > -7 && diff < 7) {
			//Finding the greatest set speed.
			
			
			this.curBot.setLeftMotorSpeed(desiredPath.get(currentDataPoint).driveSpeed);
			this.curBot.setRightMotorSpeed(desiredPath.get(currentDataPoint).driveSpeed); 
		}
		
		
		/*
		//Making and adjustment
		if(corAngle > 5 && quadAngle == 270) {
			this.testV.setLeftSpeed(testV.curLeftSpeed - .05);
		}
		
		if(corAngle > 5 && quadAngle == 0) {
			this.testV.setRightSpeed(testV.curRightSpeed - .05);
		}
		
		if(corAngle > 20 && quadAngle == 90) {
			this.testV.setRightSpeed(0);
		}
		
		if(corAngle > 20 && quadAngle == 180) {
			this.testV.setLeftSpeed(0);
		}
		
		if(corAngle < 5) {
			
			//Finding the greatest set speed.
			double setSpeed = 0;
			if(testV.curLeftSpeed > testV.curRightSpeed) {
				setSpeed = testV.curLeftSpeed;
			} else {
				setSpeed = testV.curRightSpeed;
			}
			
			
			testV.setLeftSpeed(setSpeed);
			testV.setRightSpeed(setSpeed);
		}
		*/
		
		//Deciding if I should move to the next point
		if(triToPoint.legC < desiredPath.get(currentDataPoint).pointTol) {
			
			currentDataPoint++;
			
			if(currentDataPoint < desiredPath.size()) {
				
				//System.out.println("0,0,0");
				
				processSetPoint();	//Porcessing the current setpoint.
				
			}
		}
		
		//System.out.println("Diff Angle: " + diff + ",Nav=" + navpointAngle + " Quad=" + quadAngle + ",Heading=" + currentVehicle.currentHeading );
		System.out.println("LOC=" + currentVehicle.currentX + "," + currentVehicle.currentY + ",0");
		//System.out.println(testV.curLeftSpeed + "," + testV.curRightSpeed);
		//System.out.println("CurAngle: " + corAngle + " Quad=" + quadAngle + " Heading=" + currentVehicle.currentHeading);
		
		writeLog("Diff Angle: " + diff + ",Nav=" + navpointAngle + " Quad=" + quadAngle + ",Heading=" + currentVehicle.currentHeading);
		writeLog("LOC," + currentVehicle.currentX + "," + currentVehicle.currentY + ",0");
		
		
		//End of alton
		if(currentDataPoint >= desiredPath.size()) {
			System.out.println("The End");
			this.curBot.setLeftMotorSpeed(0);
			this.curBot.setRightMotorSpeed(0);
			
			if(startingPos > 3) {
				this.curBot.setInletSpeed(1); //Setting the cube to be pushed out.
			}
			
			
			//Rough but it will run for a while.
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void processSetPoint() {

		curBot.equipmentForkLift.setDesiredSetPoint(desiredPath.get(currentDataPoint).forkLiftHeight);
		
		this.curBot.setInletSpeed(desiredPath.get(currentDataPoint).inletSpeed);
		

	}
	
	//Currently only working with sim.
	public void adjustMotors() {
		
		
	}
	
	
	public void updateCompass(double direction) {
		if(this.currentVehicle.currentHeading + direction < 0) {
			this.currentVehicle.currentHeading = 360;
		}
		
		if(this.currentVehicle.currentHeading + direction > 360) {
			this.currentVehicle.currentHeading = 0;
		}
		
		this.currentVehicle.currentHeading += direction;
		
	}
	
	//For right angles.
	public triangle solveTriangle(triangle curTriangle) {
		curTriangle.legC = Math.sqrt(Math.pow(curTriangle.legA, 2) + Math.pow(curTriangle.legB, 2));
		curTriangle.angleA = Math.toDegrees(Math.acos((double)curTriangle.legA/curTriangle.legC));
		curTriangle.angleB = 90 - curTriangle.angleA;		//The angle of the movement.
		
		return curTriangle;
	}
	
	//Solve right triangle with know hypothonose.
	public triangle solveTriagleFromHypotenuse(triangle curTriangle) {
		//Finding the opposite.
		curTriangle.legA = Math.sin(Math.toRadians(curTriangle.angleB)) * curTriangle.legC;
		curTriangle.legB = Math.cos(Math.toRadians(curTriangle.angleB)) * curTriangle.legC;
		
		//System.out.println("LegA: " + curTriangle.legA + " LegB: " + curTriangle.legB);
		
		return curTriangle;
	}
	
	private triangle triangleToPoint() {
		triangle toPoint = new triangle();
		toPoint.legA = Math.abs(desiredPath.get(currentDataPoint).pointX + currentVehicle.currentX);
		toPoint.legB = Math.abs(desiredPath.get(currentDataPoint).pointY + (currentVehicle.currentY * -1));
		toPoint.angleC = 90;
		
		toPoint = solveTriangle(toPoint);
		return toPoint;
	}
	
	//loader for the desired path.
		public void loadDesiredPath(String gameState, int startPOS) {//String fileName) {
			
			this.startingPos = startingPos;
			
			//Stright Test
			/*
			dataPath tempP = new dataPath();
			tempP.pointX = 0;
			tempP.pointY = 60;
			tempP.pointTol = 5;
			tempP.driveSpeed = -0.5;
			tempP.forkLiftHeight = -50;
			tempP.inletSpeed = -1;
			desiredPath.add(tempP);
			*/
			
			
			
			switch(startPOS) {
			case 1:
				//Left Starting Pos
				if(gameState.substring(0, 1).equals("L")) {
					dataPath tempP = new dataPath();
					tempP.pointX = -23.8215;
					tempP.pointY = 115.7081;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = 21.3694;
					tempP.pointY = 139.9305;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
				}
				
				
				if(gameState.substring(0, 1).equals("R")) {
					dataPath tempP = new dataPath();
					tempP.pointX = -23.8215;
					tempP.pointY = 115.7081;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = 22.1688;
					tempP.pointY = 207.6924;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = 164.4643;
					tempP.pointY = 235.9485;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = 177.4173;
					tempP.pointY = 195.5422;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
				}
				
				break;
				
				//Middle Starting Pos
			case 2:
				
				if(gameState.substring(0, 1).equals("L")) {
					dataPath tempP = new dataPath();
					tempP.pointX = -39.7716;
					tempP.pointY = 35.4235;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = -58.7091;
					tempP.pointY = 98.7765;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
				}
				
				
				if(gameState.substring(0, 1).equals("R")) {
					dataPath tempP = new dataPath();
					tempP.pointX = 39.7716;
					tempP.pointY = 35.4235;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = 58.7091;
					tempP.pointY = 98.7765;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
				}
				
				break;
				
				//Right Starting Pos
			case 3:
				
				if(gameState.substring(0, 1).equals("R")) {
					dataPath tempP = new dataPath();
					tempP.pointX = 23.8215;
					tempP.pointY = 115.7081;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = -21.3694;
					tempP.pointY = 139.9305;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
				}
				
				
				if(gameState.substring(0, 1).equals("L")) {
					dataPath tempP = new dataPath();
					tempP.pointX = 23.8215;
					tempP.pointY = 115.7081;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = -22.1688;
					tempP.pointY = 207.6924;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = -166.7661;
					tempP.pointY = 248.0803;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
					
					tempP = new dataPath();
					tempP.pointX = -177.4173;
					tempP.pointY = 195.5422;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
				}
				
				break;
				
			case 4:
					dataPath tempP = new dataPath();
					tempP.pointX = 0;
					tempP.pointY = 65;
					tempP.pointTol = 5;
					tempP.driveSpeed = -0.5;
					tempP.forkLiftHeight = -400;
					tempP.inletSpeed = 0;
					desiredPath.add(tempP);
				break;
				
			}
			
		}
	
	void writeLog(String dataLine) {
		
		dataLine = LocalDateTime.now().toString() + "," + dataLine + "\r\n";
				
		try {
    		fw = new FileWriter(f,true);
    		bw = new BufferedWriter(fw);
			//bw.write(dataLine);
    		bw.append(dataLine);
			bw.close();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void setupLogging() {
		
		try {
    		f = new File("/home/lvuser/" + LocalDateTime.now().toString() + "-NavOutput.csv");
    		if(!f.exists()){
    			f.createNewFile();
    		}
			fw = new FileWriter(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	bw = new BufferedWriter(fw);
	}

	/*
	@Override
	public void movementReceived(eventMovement event) {
		calcPos(event.getLeftPos(), event.getRightPos());
		
	}
	*/
}

