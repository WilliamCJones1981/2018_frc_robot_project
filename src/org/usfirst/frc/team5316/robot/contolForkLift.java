package org.usfirst.frc.team5316.robot;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.can.*;

import java.time.Duration;
import java.time.Instant;

public class contolForkLift {

	//62 inch per 1000 count. Not True!
	
	//Motor Controller.
	public TalonSRX motorForkLift;
	
	//Movement coef
	private double[] coef = new double[] {5.68434e-13, 1.13687e-13, 9.42478e+00, 5.68434e-13}; 
	private double moveTimeElement = 0.0056;
	
	//Thread for monitoring movements
	MonitorThread monitor;
	
	//Internal control states.
	private boolean driveDirection = false;		//false down, true up.
	private boolean isMoving = false;			//false stopped, true moving.
	private boolean autoMode = false;
	private double currentLocation = 0;			//0 is bottom
	private double limitTop = 5000;				//5000 is a guess.
	private double limitBottom = 0;
	private double desiredSetPoint = 0;
	
	public contolForkLift(TalonSRX motorForFork) {
		this.motorForkLift = motorForFork;
	}
	
	//Move the fork. drirect = true for up false for down. Speed sets the rate.
	public void moveFork(double speed) {
		
		isMoving = true;	//State change.
		
		monitor = new MonitorThread(this);	//Setting up tracking.
		new Thread(monitor).start();		//Starting the tracking thread.
		
		updateFork(speed);		//Updating the speed.
	}		
	
	public void stopFork() {
		this.motorForkLift.set(ControlMode.PercentOutput, 0);
		isMoving = false;
	}
	
	public void updateFork(double speed) {
		this.motorForkLift.set(ControlMode.PercentOutput, speed);
	}
	
	public double getCurrentPos() {
		return currentLocation;
	}
	
	public void setDesiredSetPoint(double pos) {
		desiredSetPoint = pos;
		autoMode = true;
		
		if(pos < currentLocation) {
			moveFork(-.5);
		}
		else
		{
			moveFork(.5);
		}
	}
	
	//To find if the fork is moving.
	public boolean isItMoving() {
		return isMoving;
	}
	
	public void setLimits(double bottom, double top) {
		this.limitBottom = bottom;
		this.limitTop = top;
	}
	
	//Thread for keep track of the fork location.
	class MonitorThread implements Runnable {
			
		contolForkLift lift;
		
		public MonitorThread(contolForkLift incomingLift) {
			this.lift = incomingLift;
		}
		
		public void run() {
			
			Instant lastInstance = Instant.now();
			
			
			//Do this while the motor is turning.
			while(isMoving) {
				//add calc distance to the loc.
				
				//Getting time passed.
				Instant curTime = Instant.now();
				Duration timeElapsed = Duration.between(lastInstance, curTime);
				
				double milSec = timeElapsed.toMillis();	//Might be a whole number not sure... Watch it.
				
				//Getting the distance traveled.
				double distanceTraveled = coef[0] * Math.pow(this.lift.motorForkLift.getMotorOutputPercent(), 3) + 
											coef[1] * Math.pow(this.lift.motorForkLift.getMotorOutputPercent(), 2) +
											coef[2] * Math.pow(this.lift.motorForkLift.getMotorOutputPercent(), 1) +
											coef[3];
				
				this.lift.currentLocation += distanceTraveled;
				
				//Updating the lastTime data.
				lastInstance = curTime;
				
				double span = 10;
				
				if(autoMode) {
					
					
					if(this.lift.currentLocation > desiredSetPoint - span && this.lift.currentLocation < desiredSetPoint + span) {
						this.lift.stopFork();
					}
				}
				
				//System.out.println("Lift=" + this.lift.currentLocation);
				
				//Sleeping thread to no thread lock the system.
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			
			
		}
	}	
	
	
}
