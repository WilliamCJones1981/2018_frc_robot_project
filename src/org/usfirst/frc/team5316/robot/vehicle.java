package org.usfirst.frc.team5316.robot;


public class vehicle {

	//Vehicle details.
	public double length; //Length of the vehicle. In the Y Axis. In inches.
	public double width;	//Width of the vehicle. X Axis.  In inches.
	public double height;	//Height of the vehicle.  In inches.
	public double wheelBase;	//Distance from the center of the wheel from side to side along the X axis. In Inches.
	public double wheelLeftDia;		//Diameter of the left drive wheel. In Inches.
	public double wheelRightDia;	//Diameter of the right drive wheel. In Inches.
	public double encoderLeftCount;
	public double encoderRightCount;
	public double liftUpRate;		//Rate the lift moves up under load. Inches/Second
	public double liftDownRate;		//Rate the left moves down not loaded. Inches/Second
	public double encoderRotation;	//Encoder value for one full rotation of the shaft.
	
	//Vehicle current position information.
	public double currentX;		//Current X position
	public double currentY;		//Current Y position.
	public double currentHeading;		//Current heading with 0 bring straight forward at start.
	public double curLeftSpeed;		//Speed of current left side
	public double curRightSpeed;	//Speed of right side.
	
	
	public vehicle() {
		length = 32.25;
		width = 27.625;
		height = 50;
		wheelBase = 24.125;
		wheelLeftDia = 6;
		wheelRightDia = 6;
		encoderRotation = 4096;
		
		//Setting starting pos to x0 y0
		currentX = 0;
		currentY = 0;
		currentHeading = 0;
		curLeftSpeed = 0;
		curRightSpeed = 0;
	}
	
	public void updateX(double X) {
		currentX = X;
	}
	
	public void updateY(double Y) {
		currentY = Y;
	}
	
	public double getTavel(double encoderAmount, String curSide) {
		double ratioOfRotation = encoderAmount / this.encoderRotation;
		
		switch(curSide) {
		case "right":
			return (2 * Math.PI * (wheelRightDia/2)) * ratioOfRotation;
			
		case "left":
			return (2 * Math.PI * (wheelLeftDia/2)) * ratioOfRotation;
		}
		
		return 0;
		
	}
		
	
}
